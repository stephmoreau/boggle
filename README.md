## Boggle Style Game ##
Using only Html and CSS to create the board for the game. 
JS to be added later to create a more dynamic game.

---

## Extra files used
List of files besides out index.html and style.css
1. Google Fonts to make it look a little nicer


---

## JavaScript
JavaScript will be included for the following:
1. Connection to an API to validate if the words submitted are real.
2. Randomize the board
3. Count score

---

## Future Tasks ##
Notes on possible options/functionality that could be added

1. CSS disabled form element
2. CSS animate the dice rolling? (chnage the letter on the dice automatically)
3. Could implement cross browser compatibility, but really only testing on Chrome for MacBook! (highly doubtful)