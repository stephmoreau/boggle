/** Script for boggle game **/

// once dom is loaded, initialize the game board
document.onload = initBoard();

var diceLetters, genLetters;
var guessedWord = [];
var ptsScore, ptsWords = 0;
const NUM_DICE = 16;

/**
 * Summary. initBoard
 * Description. Initialize the game board so we can start playing the game
 *
 */
function initBoard(){
  diceLetters = document.getElementsByClassName('letter');
  
  // clear game board
  resetBoard();
  
  //setup listener to start game
  document.getElementById('btn_play').addEventListener('click', startGame);

}

function resetBoard(){
  // reset score
  updateScore();
  // remove every letter / roll the dice
  for (let a = 0; a < diceLetters.length; a++){
    diceLetters[a].innerHTML = "";
  }
  
  // remove all guessed words
  document.getElementById('listing').innerHTML = "";
  
  // clear form elements
  document.getElementById('txt_guess').value = "";
  
  //disable form elements
  document.getElementById('txt_guess').setAttribute('disabled','disabled');
  document.getElementById('btn_guess').setAttribute('disabled','disabled');
  
  //enable start button
  document.getElementById('btn_play').removeAttribute('disabled');
}

function updateScore(wordLength = null){
  ptsWords++;
  
  switch (wordLength){
    case null :
      ptsScore = ptsWords = 0;
      break;
    case 0: break;
    case 1:
    case 2:
    case 3:
    case 4 : ptsScore += 1; break; 
    case 5 : ptsScore += 2; break; 
    case 6 : ptsScore += 3; break; 
    case 7 : ptsScore += 5; break; 
    default : ptsScore += 11;
  }

  document.getElementById('wordsFound').childNodes.item(1).innerHTML = ptsWords;
  document.getElementById('points').childNodes.item(1).innerHTML = ptsScore;
}

function startGame(){
  
  try{
    // setup letters - id < 2 vowels, regenerate
    do {
      genLetters = generateLetters();
    }while( genLetters.match(/[aeiou]/g).length < 2);

  }catch (e){
    return;  
  }
  for (let l = 0; l < diceLetters.length; l++){
    diceLetters[l].innerHTML = genLetters[l];
    diceLetters[l].setAttribute('id', "dice_" + l);
    diceLetters[l].addEventListener('click', diceClicked);
  }
  
  //disable form elements
  document.getElementById('btn_play').setAttribute('disabled','disabled');

  //enable start button
  document.getElementById('txt_guess').removeAttribute('disabled');
  document.getElementById('txt_guess').focus();
  document.getElementById('btn_guess').removeAttribute('disabled');
  
  document.getElementById('current').addEventListener('submit', submitGuess);
}

function submitGuess(){
  
  //save current word
  let guess = document.getElementById('txt_guess').value;
  let hasError = false;

  // prevent guessing while validating
  document.getElementById('txt_guess').setAttribute('readonly', 'readonly');
  document.getElementById('btn_guess').setAttribute('readonly', 'readonly');
  
  
  // add guess to list of words
  let newWord = document.createElement( 'span' );
  newWord.appendChild( document.createTextNode( guess ) );
  newWord.setAttribute( 'id', 'guess_' + guess );

  // validate word - word already guessed
  if (guessedWord.includes( newWord )){
    hasError = true;
    newWord.className = "wrongLetter";
    updateScore( 0 );
  }else{  
    //validate word - letters founds on dice
    var tmp = genLetters.split("");
    for (i=0; i<guess.length; i++){
      let index = tmp.indexOf(guess[i]);
      if (index === -1){
        hasError = true;
        newWord.className = "wrongLetter";
        updateScore( 0 );
        break;
      }
      tmp.splice(index,1);
    }
  
    if (!hasError){  
    //valiate word - check api
      newWord.className = "waiting";
     
      performAjaxRequest(guess);
    }
     
  }

  document.getElementById('listing').appendChild( newWord );
  guessedWord.push( newWord );

  // clear input
  document.getElementById('txt_guess').value = "";
  
  // allow guessing
  document.getElementById('txt_guess').removeAttribute('readonly');
  document.getElementById('txt_guess').focus();
  document.getElementById('btn_guess').removeAttribute('readonly');
  
}

function performAjaxRequest( word ){
  
  let url = new URL('http://localhost/api/word/exists.php');
  url.searchParams.set('word', word);
  
  let xhr = new XMLHttpRequest();
  
  xhr.open('GET', url);
  xhr.responseType = 'json';
  xhr.setRequestHeader('api_key', 'IPD');
  
  xhr.send();

  xhr.onload = function() {
  console.log(xhr.response);
    if (xhr.status == 200) { // word is good
      
      document.getElementById('guess_' + word).className = "accepted"
      updateScore( word.length );
    } else { // show the result
      
      document.getElementById('guess_' + word).className = "notFound";
      updateScore( 0 );
    }
    
    

  
  };      

   xhr.onerror = function() {
     updateScore( 0 );

  };
}

function diceClicked(){
  document.getElementById('txt_guess').value = document.getElementById('txt_guess').value + document.getElementById(this.id).innerHTML;
  document.getElementById('txt_guess').focus();
}

/**
 * Summary. generateLetters
 * Description. Recursive function to generate the ramdom letters of our board
 * {params} lenght: lenght of string to generate
 * {params} current: the current string being generated
 * {return} newly generated string
 */// function inspiration : https://stackoverflow.com/a/5885493 :)
function generateLetters(length = NUM_DICE, current) {
  current = current ? current : '';
  return length ? generateLetters(--length, "qwertyui\ppplkjhgfdszxcvbnm".charAt(Math.floor(Math.random() * 26)) + current) : current;
}
